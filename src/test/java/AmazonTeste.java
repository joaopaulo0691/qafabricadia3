import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AmazonTeste {

    private WebDriver driver;

    @Before
    public void abrir(){

        System.setProperty("webdriver.gecko.driver", "C:/Users/João Paulo/Documents/qafabricadia3/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize(); //quando o teste abrir, abrirá em tela cheia
        driver.get("https://www.amazon.com.br/");
    }

    @After
    public void sair(){
        driver.quit();
    }

    @Test
    public void maisVendidos() throws InterruptedException {
        Thread.sleep(6000);
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(6000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[2]/a")).click();
        Thread.sleep(6000);
        Assert.assertEquals("Mais vendidos", driver.findElement(By.xpath("//*[@id=\"zg_banner_text\"]")).getText());
        Thread.sleep(6000);
    }

    @Test
    public void testarBuscador() throws InterruptedException {
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Qualquer");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Thread.sleep(6000);
        /* para comparar usar Assert.assertEquals, veja que ele compara uma palavra esperada
          (ATENÇÃO! essa palavra esperada tem que ser igual como está no site), com o caminho dado pela xpath.
        */
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
        Thread.sleep(6000);
    }

    @Test
    public void testarAutenticacaoSenhaIncorreta() throws InterruptedException {
        driver.findElement(By.id("nav-link-accountList")).click();
        driver.findElement(By.id("ap_email")).sendKeys("kandjaddeer@gmail.com");
        driver.findElement(By.id("continue")).click();
        // validando o teste
        Assert.assertEquals("Não encontramos uma conta associada a este endereço de e-mail", driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[1]/div/div/div/div/ul/li/span")).getText());
        Thread.sleep(6000);
    }

    @Test
    public void buscarLivros() throws InterruptedException {
        driver.findElement((By.className("nav-a"))).click();
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Java");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
        Thread.sleep(6000);
    }

    @Test
    public void clicarAbaTodos() throws InterruptedException {
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[3]/a")).click();
        Assert.assertEquals("Novidades na Amazon em Móveis", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[2]/div/div/div/div[2]/div/div[1]/div/div[1]/div/div/div/div/div[1]/div[1]/h2")).getText());
        Thread.sleep(6000);
    }

    @Test
    public void clicarOfertasDoDia() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div/header/div/div[4]/div[2]/div[2]/div/a[4]")).click();
        Assert.assertEquals("Ofertas e Promoções", driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[2]/div/div/h1")).getText());
        Thread.sleep(6000);
    }

    @Test
    public void clicarCarrinhoVazio() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[1]/div[3]/div/a[3]/div[2]/span[2]")).click();
        Thread.sleep(6000);
    }



}
